﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using testing.Dto;

namespace testing.Models
{
    public class Cuota
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public MesEnum Mes { get; set; }

        [Required]
        public int Anio { get; set; }

        public int UsuarioId { get; set; }
        public virtual Usuario? Usuario { get; set; }

    }
}
