using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using testing.Dto;

namespace testing.Models
{
    public class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required, MinLength(1), MaxLength(10)]
        public string? Name { get; set; }

        [Required]
        public string? Email { get; set; }

        [Required]
        public string? Password { get; set; }

        [Required]
        public bool? IsAdmin { get; set; }

        [Required]
        public RolEnum Rol {  get; set; }

        public virtual List<Cuota>? Cuotas { get; set; }

    }
}
