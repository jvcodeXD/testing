using Microsoft.EntityFrameworkCore;
using testing.Models;

namespace testing.Context
{
    public class MyContext: DbContext
    {
        public MyContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }
        
        public DbSet<Cuota> Cuotas { get; set; }

    }
}
